--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  `status` enum('todo','doing','done') NOT NULL,
  `reminder` timestamp NULL DEFAULT NULL
)
ENGINE=InnoDB;