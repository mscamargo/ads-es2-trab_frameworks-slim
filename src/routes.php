<?php

use Slim\Http\Request;
use Slim\Http\Response;
use App\Controllers\TaskController;

// Routes

$app->get('/tasks', function (Request $request, Response $response) {
    return TaskController::index($request, $response);
});

$app->post('/tasks', function (Request $request, Response $response) {
    return TaskController::store($request, $response);
});

$app->get('/tasks/{id}', function (Request $request, Response $response, $args) {
    return TaskController::show($args['id'], $response);
});

$app->put('/tasks/{id}', function (Request $request, Response $response, $args) {
    return TaskController::update($args['id'], $request, $response);
});

$app->delete('/tasks/{id}', function (Request $request, Response $response, $args) {
    return TaskController::destroy($args['id'], $response);
});