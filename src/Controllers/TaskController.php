<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\Task;

class TaskController
{
    public static function index(Request $request, Response $response): Response
    {
        $tasks = Task::all();
        if (empty($tasks) || count($tasks) == 0) {
            return $response->withStatus(204);
        }
        return $response->withJson($tasks, 200);
    }

    public static function store(Request $request, Response $response): Response
    {
        $task = Task::create($request->getParsedBody());
        if (empty($task)) {
            return $response->withStatus(500);
        }
        return $response->withJson($task, 201);
    }

    public static function show(int $id, Response $response): Response
    {
        $task = Task::find($id);
        if (empty($task)) {
            return $response->withStatus(404);
        }
        return $response->withJson($task, 200);
    }

    public static function update(int $id, Request $request, Response $response): Response
    {
        $task = Task::find($id);
        if (empty($task)) {
            return $response->withStatus(404);
        }
        if (!$task->update($request->getParsedBody())) {
            return $response->withStatus(500);
        }
        return $response->withStatus(200);
    }

    public static function destroy(int $id, Response $response): Response
    {
        $task = Task::find($id);
        if (empty($task)) {
            return $response->withStatus(404);
        }
        if (!$task->delete()) {
            return $response->withStatus(500);
        }
        return $response->withStatus(204);
    }
}